# Set master image
FROM php:7.4-fpm-alpine

LABEL maintainer="Dodypras (prasetyodody17@gmail.com)"

# Set working directory
WORKDIR /var/www/html

# Install Additional dependencies
RUN apk update && apk add --no-cache \
    build-base shadow supervisor \
    php7-common \
    php7-openssl \
    php7-json \
    php7-phar \
    php7-zip \
    php7-gd \
    php7-dom \
    php7-session \
    php7-zlib \
    php7-opcache \
    php7-session \
    php7-dom \
    php7-xml \
    php7-xmlreader \
    php7-ctype \
    php7-ftp \
    php7-gd \
    php7-json \
    php7-posix \
    php7-curl \
    php7-pdo \
    php7-pdo_mysql \
    php7-sockets \
    php7-zlib \
    php7-mcrypt \
    php7-mysqli \
    php7-bz2 \
    php7-phar \
    php7-openssl \
    php7-posix \
    php7-zip \
    php7-calendar \
    php7-iconv \
    php7-imap \
    php7-soap \
    php7-dev \
    php7-pear \
    php7-redis \
    php7-mbstring \
    php7-exif \
    php7-xsl \
    php7-ldap \
    php7-bcmath \
    php7-oauth \
    php7-apcu \
    php7-intl \
    php7-xmlrpc \
    php7-simplexml \
    php7-fileinfo \
    bash \
    libressl \
    ca-certificates \
    openssh-client \
    libzip-dev \
    libcurl \
    curl-dev \
    libpng-dev \
    gettext-dev \
    icu-dev \
    libxml2-dev 

# Add and Enable PHP-PDO Extenstions
RUN docker-php-ext-install pdo pdo_mysql mysqli curl gd gettext iconv intl soap xmlrpc zip
RUN docker-php-ext-enable pdo_mysql mysqli curl gd gettext iconv intl soap xmlrpc zip
RUN docker-php-ext-install pcntl

# Remove Cache
RUN rm -rf /var/cache/apk/*

COPY .docker/supervisord.conf /etc/supervisord.conf
COPY .docker/supervisor.d /etc/supervisor.d

# Use the default dev configuration
COPY .docker/php/php.ini /usr/local/etc/php/php.ini

# Add UID '1000' to www-data
RUN usermod -u 1000 www-data

# Copy existing application directory
COPY . .

RUN chown -R www-data:www-data .

# ENV ENABLE_CRONTAB 1
# ENV ENABLE_HORIZON 1
# ENV ENABLE_WORKER 1
ENV CURRENT_USER www-data

ENTRYPOINT ["sh", "/var/www/html/.docker/docker-entrypoint.sh"]

CMD supervisord -n -c /etc/supervisord.conf